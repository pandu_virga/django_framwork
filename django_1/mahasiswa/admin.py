from django.contrib import admin
from mahasiswa.models import *

# Register your models here.
# Definisi pengelolaan data mahasiswa yang ditampilkan dihalaman Admin
class MahasiswaAdmin(admin.ModelAdmin):
    list_display = ['nim','nama','alamat','jk','ps','no_telp','email']
    list_filter = ('nim','nama','ps','jk')
    search_fields = ['nim','nama','ps','jk']
    list_per_page = 100

# mengaitkan class MahasiswaAdmin dengan modul Mahasiswa
admin.site.register(Mahasiswa, MahasiswaAdmin)