from django.shortcuts import render
from . import models # import modul models

# query set mengambil semua data di table model mahasiswa
dataMhs = models.Mahasiswa.objects.all()

# Create your views here.
context = {'judul':'Web Mahasiswa',
           'pageName':'Halaman Tampil Mahasiswa',
           'kontributor':'Pandu Virga Pradana',
           'mahasiswas':dataMhs}

# Create your views here
def index(request):
    return render(request,'mahasiswa/index.html',context)
